
import { LOGIN_SUCCESS, LOGOUT_SUCCESS } from '../actions/password/login';

const localize = ({dispatch, getStore})=>(next)=>(action)=>{
      if(action || action.type === LOGIN_SUCCESS){
          window.localStorage.setItem("user", action.username);
      }
      if(action || action.type === LOGOUT_SUCCESS){
        window.localStorage.removeItem("user");
      }
};

export default localize;