import './Root.less'  //引入antd的样式
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux'

import App from './containers/App'
import Login from './containers/password/login';

import createStore from "./store/store"

const store = createStore();

const Root = ()=>{
    return (
        <Provider store = {store}>
            <BrowserRouter>
                <Switch>
                    <Route path="/login" component={Login}/>
                    <Route path="/" component={App} />
                </Switch>
            </BrowserRouter>
        </Provider>
    )
};

export default Root;