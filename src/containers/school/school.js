// type为网上党课类型的页面
//
 import React from 'react';
 import {Switch, Route, withRouter} from 'react-router-dom';
 
 import Main from './main'
/**
 * 需要页面的api, 得到左侧菜单列表和页面信息，不知道有没有
 *  获取面
 */
const test =  [{
        "name":"申请人培训",
        "url": "/school/applicant"
    }, 
    {
        name: '积极分子培训',
        'url':"/school/activist"
    }]
 const School = ({match, location, history}) =>{

     return (
       <div>
         {/**  <SideMenu></SideMenu> */}
         <Switch>
          { test.map( (item, key)=>{
               return (
                       <Route path={`${item.url}`} render={()=>(<Main  url={item.url}/>)} key={key}/>
                      
               )
         })} 
         </Switch>  
      </div>);

 };
 export default withRouter(School);