import React from 'react';
import { Switch, Route, withRouter, Link } from 'react-router-dom';

import Course from './course';
import Test from './test'

const Main = ({url})=>{
    const course_path = `${url}/course`;
    const test_path = `${url}/test`;
    console.log(course_path)
    return (
      <div>
        <Link to={course_path}>课程</Link>
        <hr></hr>
        <Link to={test_path}>考试</Link>
        <Switch>
            <Route path={course_path} component={ Course }/>
            <Route path={test_path} component={ Test }/>
        </Switch>
      </div>
    )
}
export default  withRouter(Main);