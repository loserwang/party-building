import React from 'react'
import {Route, Switch } from 'react-router-dom';


import Header from './frame/header';
import Footer from '../components/frame/footer';

import Index from '../containers/index/index'
import School from '../containers/school/school';
import Branch from '../containers/branch/branch'
import News   from '../containers/news/News'
import Document  from '../containers/document/document'

const testMenu = [
    {url:"/school",name:"我的党校",type:"school",subMenu: [{
                "name":"申请人培训",
                "url": "/school/applicant"
            }, 
            {
                name: '积极分子培训',
                'url':"/school/activist"
            }]}, //是否需要子菜单,如果需要，是否给出url
    {url:"/branch", name:"我的支部", type:"branch",subMenu: [{
                "name":"个人状态",
                "url": "/branch/myStatus"
            }, 
            {
                name: '支部情况',
                'url':"/branch/myBranch"
            },{
                name: '我的信息',
                'url':"/branch/message"
            }]},
    {url:"/notice", name:"通知公告", type:"news",
            subMenu: [{
                "name":"通知公告",
                "url": "/notice"
            }]},
     {url:"/theory", name:"理论学习", type:"news",
            subMenu: [{
                "name":"经典理论",
                "url": "/classic"
            }]},        
    {url:"/document", name:"重要文件", type:"document", subMenu:[{
        "name":"我的文件",
        "url": "/document"
    }]},

]
const App = ()=>{
    console.log("APP")
    if(!testMenu) return  // loading....
    return (
        <div className="part-building">
            <Header menu={testMenu}/>
            <Switch>
                  <Route exact path={"/"} component={Index}></Route>
               { testMenu.map( (item, key) =>{
                         //根据页面的类型来找到对应的组件: school类、branch类、document类和news类四种
                         switch(item.type){
                             case "school":
                                   return  <Route path={item.url} key={key} component={School} />
                             case "branch":
                                   return  <Route path={item.url} key={key} component={Branch} />
                             case "news":
                                   return  <Route path={item.url} key={key} component={News} />
                             case "document":
                                   return  <Route path={item.url} key={key} component={Document} />
                             
                                 
                         }
                   return <Route path={item.url} key={key} component={Header} />
               } )}
               
            </Switch>
            <Footer />
        </div>
    )
}
export default App;