import React from 'react';
import {Link, Redirect} from 'react-router-dom'
import Menu from 'antd/lib/menu';
import SubMenu from 'antd/lib/menu/SubMenu'
const testMenu = [
    {url:"/school",name:"我的党校",type:"school",subMenu: [{
                "name":"申请人培训",
                "url": "/school/applicant"
            }, 
            {
                name: '积极分子培训',
                'url':"/school/activist"
            }]}, //是否需要子菜单,如果需要，是否给出url
    {url:"/branch", name:"我的支部", type:"branch"},
    {url:"/news", name:"通知公告", type:"news"},
    {url:"/document", name:"重要文件", type:"document"},

]
//参数分别为菜单列表， 用户名， 退出方法    ？？有退出方法不好吧
const Header  = ({menu, username, logout}) => {
      console.log(menu)
     return (
         <div className = "header">
               <img src="logo"/>
               <Menu mode="horizontal" >
                    {
                         //还要加个检测
                        menu.map((item, index)=>(
                           <SubMenu title={item.name} key={index}>
                             
                               {item.subMenu? item.subMenu.map( (item1, index1)=>(item1?
                                  
                                       <Menu.Item >
                                         <Link to={item1.url}>{item1.name}</Link>
                                       </Menu.Item>: null
                               )):null} 
                           </SubMenu>
                    ))}
                
             </Menu>          
               {username? 
                   <Link style={{float:"right"}} to="/login">
                      登录
                  </Link>:<div>{username}<span onClick={logout}>退出</span></div>}
         </div>
     );
};

export default Header;