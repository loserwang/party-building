import { createStore, applyMiddleware, compose } from 'redux'
import reduxThunk from 'redux-thunk'
import reduxLogger from 'redux-logger'

import rootReducer from '../reducers/root'
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
 const store = (preloadedState)=> 
                  createStore(rootReducer, preloadedState,  composeEnhancers(
                    applyMiddleware(reduxThunk,reduxLogger))
                  );

export default store;